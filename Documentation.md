# ATT Pre-Popup (Optional)

## Introduction

For iOS apps, ATT Pre-Popup can be used to prepare user to accept consent, by providing user-friendly information about what and why we need to collect data.

## Integration Steps

1) **"Install"** or **"Upload"** FG ATT Pre-Popup plugin from the FunGames Integration Manager in Unity, or download it from here.

2) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.